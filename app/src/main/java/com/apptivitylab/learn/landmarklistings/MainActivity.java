package com.apptivitylab.learn.landmarklistings;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.apptivitylab.learn.landmarklistings.ui.LandmarkListingFragment;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.activity_main_vg_container, new LandmarkListingFragment())
                .commit();
    }
}
