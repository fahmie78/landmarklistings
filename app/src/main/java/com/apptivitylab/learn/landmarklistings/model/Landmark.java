package com.apptivitylab.learn.landmarklistings.model;

import java.util.ArrayList;

/**
 * Created by aarief on 22/09/2016.
 */

public class Landmark {
    private String mName;
    private String mLocation;
    private ArrayList<Review> mReviews;

    public Landmark(String name, String location) {
        mName = name;
        mLocation = location;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getLocation() {
        return mLocation;
    }

    public void setLocation(String location) {
        mLocation = location;
    }

    public ArrayList<Review> getReviews() {
        return mReviews;
    }

    public void setReviews(ArrayList<Review> reviews) {
        mReviews = reviews;
    }

    @Override
    public String toString() {
        return getName();
    }

    public double getAverageReview() {
        double average = 0.0;

        for (Review review:mReviews) {
            average += review.getRating();
        }

        return average / (double)mReviews.size();
    }

}
