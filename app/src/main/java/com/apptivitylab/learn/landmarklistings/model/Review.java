package com.apptivitylab.learn.landmarklistings.model;

/**
 * Created by aarief on 22/09/2016.
 */

public class Review {
    private int mRating;

    public Review(int rating) {
        mRating = rating;
    }

    public int getRating() {
        return mRating;
    }

    public void setRating(int rating) {
        mRating = rating;
    }
}
