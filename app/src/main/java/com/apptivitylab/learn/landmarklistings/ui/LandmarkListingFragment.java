package com.apptivitylab.learn.landmarklistings.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.apptivitylab.learn.landmarklistings.R;
import com.apptivitylab.learn.landmarklistings.model.Landmark;
import com.apptivitylab.learn.landmarklistings.model.Review;

import java.util.ArrayList;

/**
 * Created by aarief on 21/09/2016.
 */

public class LandmarkListingFragment extends Fragment {
    private ListView mListView;
    private ArrayList<Landmark> mLandmarkList;

    private String[] mLandmarkNames = {"Statue of Liberty",
            "Eiffel Tower",
            "Sydney Opera House",
            "Machu Picchu",
            "St. Peter's Basilica",
            "Taj Mahal",
            "Great Wall of China",
            "Golden Gate Bridge"};
    private String[] mLocations = {"New York",
            "Paris",
            "Sydney",
            "Peru",
            "Rome",
            "India",
            "China",
            "San Francisco"};

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_landmark_listing, container, false);

        mListView = (ListView) rootView.findViewById(R.id.fragment_landmark_listing_listview);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mLandmarkList = new ArrayList<>();

        for (int i = 0; i < mLandmarkNames.length; i++) {
            Landmark landmark = new Landmark(mLandmarkNames[i], mLocations[i]);
            landmark.setReviews(generateRandomReviewList());

            mLandmarkList.add(landmark);
        }

        ArrayAdapter arrayAdapter = new ArrayAdapter(getContext(), android.R.layout.simple_list_item_1, mLandmarkList);
        mListView.setAdapter(arrayAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Landmark selectedLandmark = mLandmarkList.get(i);
//                String snackbarMessage = "Hello " + selectedLandmark.getName() + "! See you in " + selectedLandmark.getLocation();
                String snackbarMessage = "Average review of " + selectedLandmark.getName() + " is " + selectedLandmark.getAverageReview();

                Snackbar.make(view, snackbarMessage, Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    private ArrayList<Review> generateRandomReviewList() {
        ArrayList<Review> randomList = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            randomList.add(new Review((int) (Math.random() * 5) + 1));
        }

        return randomList;
    }

}
